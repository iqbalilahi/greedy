<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_barang');
	}

	public function index()
	{
		$barang = $this->M_barang->Read();
		$data = array(
			'barang' => $barang,
			'isi'    => "Barang/barang");
		$this->load->view('Layout/layout', $data, FALSE);
		
	}

	public function FormCreate()
	{
		$data = array(
			'isi' => "Barang/barang-tambah");
		$this->load->view('Layout/layout', $data, FALSE);
	}

	public function Create()
	{
		$nama_barang  = $this->input->post('nama_barang');
		$jenis_barang = $this->input->post('jenis_barang');

		$data = array(
			'nama_barang'  => $nama_barang,
			'jenis_barang' => $jenis_barang);

		$this->M_barang->Create($data);
		$this->session->set_flashdata('sukses', ' Data Berhasil Di Tambahkan');
		redirect(base_url('Barang'),'refresh');
	}

	public function Update()
	{
		$id_barang    = $this->input->post('id_barang');
		$nama_barang  = $this->input->post('nama_barang');
		$jenis_barang = $this->input->post('jenis_barang');

		$data = array(
			'id_barang'    => $id_barang,
			'nama_barang'  => $nama_barang,
			'jenis_barang' => $jenis_barang);

		$this->M_barang->Update($data);
		$this->session->set_flashdata('sukses', ' Data Berhasil Di Edit');
		redirect(base_url('Barang'),'refresh');
	}

	public function FormUpdate($id_barang)
	{
		$barang = $this->M_barang->Detail($id_barang);
		$data = array(
			'barang' => $barang,
			'isi'    => "Barang/barang-edit");
		$this->load->view('Layout/layout', $data, FALSE);
	}

	public function Delete($id_barang)
	{
		$data = array('id_barang' => $id_barang);
		$this->M_barang->Delete($data);
		$this->session->flashdata('sukses', 'Data Telah Di Hapus !!!');

		redirect(base_url('Barang'),'refresh');
	}


}
