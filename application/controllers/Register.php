<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_register');
	}

	public function index()
	{

		$data = array('tittle' => 'Administrator');
		$this->load->view('register.php', $data, FALSE);

		$valid = $this->form_validation;

		$valid->set_rules('nama_depan', 'Nama Depan', 'required',
			array('required'  => 'Nama Depan Harus Di Isi'));

		$valid->set_rules('nama_belakang', 'Nama Belakang','required',
			array('required' => 'Nama Belakang Harus Di Isi'));


		$valid->set_rules('username', 'Username','required|is_unique[login.username]',
			array('required'  => 'Username Harus Di Isi',
				  'is_unique' => 'Username <strong>'.$this->input->post('username').'</strong> Sudah Ada'));

		$valid->set_rules('password','Password','required',
			array('required'   => 'Password Harus Di Isi'));

	
		if ($valid->run()===FALSE) {
			
			// $data = array('tittle' => 'Login Administrator');
			// $this->load->view('V-register.php', $data, FALSE);

		}else{

			$insert = $this->input;
			$data = array('nama_depan'		=>$insert->post('nama_depan'),
						  'nama_belakang'	=>$insert->post('nama_belakang'),
						  'username'		=>$insert->post('username'),
						  'password'		=>$insert->post('password'));

			$this->M_register->Register($data);
			$this->session->set_flashdata('sukses', ' Registered Berhasil');
			redirect(base_url('Login'),'refresh');


		}
		
		
	}

}
