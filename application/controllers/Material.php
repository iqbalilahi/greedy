<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Material extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_material');
	}

	public function index()
	{

		$material = $this->M_material->Read();
		$data = array(
			'material' => $material,
			'isi'      => "Material/material");
		$this->load->view('Layout/layout', $data, FALSE);
		
	}

	public function FormCreate()
	{
		$kode = $this->M_material->Kode();
		$data = array(
			'kode' => $kode,
			'isi'  => "Material/material-tambah");
		$this->load->view('Layout/layout', $data, FALSE);
	}

	public function Create()
	{
		$kode_part        = $this->input->post('kode_part');
		$nama_material    = $this->input->post('nama_material');
		$panjang_material = $this->input->post('panjang_material');

		$data = array(
			'kode_part'        => $kode_part,
			'nama_material'    => $nama_material,
			'panjang_material' => $panjang_material);

		$this->M_material->Create($data);
		$this->session->set_flashdata('sukses', ' Data Berhasil Di Tambahkan');
		redirect(base_url('Material'),'refresh');
	}

	public function Update()
	{
		$id_material      = $this->input->post('id_material');
		$kode_part        = $this->input->post('kode_part');
		$nama_material    = $this->input->post('nama_material');
		$panjang_material = $this->input->post('panjang_material');

		$data = array(
			'id_material'      => $id_material,
			'kode_part'        => $kode_part,
			'nama_material'    => $nama_material,
			'panjang_material' => $panjang_material);

		$this->M_material->Update($data);
		$this->session->set_flashdata('sukses', ' Data Berhasil Di Edit');
		redirect(base_url('Material'),'refresh');
	}

	public function FormUpdate($id_material)
	{
		$material = $this->M_material->Detail($id_material);
		$data = array(
			'material' => $material,
			'isi'      => "Material/material-edit");
		$this->load->view('Layout/layout', $data, FALSE);
	}

	public function Delete($id_material)
	{
		$data = array('id_material' => $id_material);
		$this->M_material->Delete($data);
		$this->session->flashdata('sukses', 'Data Telah Di Hapus !!!');

		redirect(base_url('Material'),'refresh');
	}

}
