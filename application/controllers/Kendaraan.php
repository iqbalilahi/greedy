<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kendaraan extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_kendaraan');
	}

	public function index()
	{

		$kendaraan = $this->M_kendaraan->Read();
		$data = array(
			'kendaraan' => $kendaraan,
			'isi' 		=> "Kendaraan/kendaraan");

		$this->load->view('Layout/layout', $data, FALSE);
		
	}

	public function FormCreate()
	{

		$data = array(
			'isi' 		=> "Kendaraan/kendaraan-tambah");

		$this->load->view('Layout/layout', $data, FALSE);
		
	}

	public function Create()
	{
		$nama_kendaraan    = $this->input->post('nama_kendaraan');
		$panjang_kendaraan = $this->input->post('panjang_kendaraan');
		$lebar_kendaraan   = $this->input->post('lebar_kendaraan');
		$tinggi_kendaraan  = $this->input->post('tinggi_kendaraan');

		$data = array(
			'nama_kendaraan'    => $nama_kendaraan,
			'panjang_kendaraan' => $panjang_kendaraan,
			'lebar_kendaraan'   => $lebar_kendaraan,
			'tinggi_kendaraan'  => $tinggi_kendaraan);

		$this->M_kendaraan->Create($data);
		$this->session->set_flashdata('sukses', ' Data Berhasil Di Tambahkan');
		redirect(base_url('Kendaraan'),'refresh');


	}

	public function FormUpdate($id_kendaraan)
	{

		$kendaraan = $this->M_kendaraan->Detail($id_kendaraan);
		$data = array(
			'kendaraan' => $kendaraan,
			'isi' 		=> "Kendaraan/kendaraan-edit");

		$this->load->view('Layout/layout', $data, FALSE);
		
	}

	public function Update()
	{
		$id_kendaraan      = $this->input->post('id_kendaraan');
		$nama_kendaraan    = $this->input->post('nama_kendaraan');
		$panjang_kendaraan = $this->input->post('panjang_kendaraan');
		$lebar_kendaraan   = $this->input->post('lebar_kendaraan');
		$tinggi_kendaraan  = $this->input->post('tinggi_kendaraan');

		$data = array(
			'id_kendaraan'      => $id_kendaraan,
			'nama_kendaraan'    => $nama_kendaraan,
			'panjang_kendaraan' => $panjang_kendaraan,
			'lebar_kendaraan'   => $lebar_kendaraan,
			'tinggi_kendaraan'  => $tinggi_kendaraan);

		$this->M_kendaraan->Update($data);
		$this->session->set_flashdata('sukses', ' Data Berhasil Di Edit');
		redirect(base_url('Kendaraan'),'refresh');


	}

	public function Delete($id_kendaraan)
	{
		$data = array('id_kendaraan' => $id_kendaraan);
		$this->M_kendaraan->Delete($data);
		$this->session->flashdata('sukses', 'Data Telah Di Hapus !!!');

		redirect(base_url('Kendaraan'),'refresh');
	}

}
