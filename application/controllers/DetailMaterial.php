<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DetailMaterial extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_detail_material');
		$this->load->model('M_material');
		$this->load->model('M_supplier');
	}

	public function index()
	{
		$DetailMaterial = $this->M_detail_material->Read();
		$data = array(
			'DetailMaterial' => $DetailMaterial,
			'isi'            => "DetailMaterial/detail-material");

		$this->load->view('Layout/layout', $data, FALSE);
	}

	public function FormCreate()
	{

		$material = $this->M_material->Read();
		$supplier = $this->M_supplier->Read();
		$data = array(
			'supplier' => $supplier,
			'material' => $material,
			'isi'      => "DetailMaterial/detail-material-tambah");

		$this->load->view('Layout/layout', $data, FALSE);
		
	}

	public function Create()
	{
		$id_material    = $this->input->post('id_material');
		$id_supplier    = $this->input->post('id_supplier');
		$harga_material = $this->input->post('harga_material');

		$data = array(
			'id_material'    => $id_material,
			'id_supplier'    => $id_supplier,
			'harga_material' => $harga_material);

		$this->M_detail_material->Create($data);
		$this->session->set_flashdata('sukses', ' Data Berhasil Di Tambahkan');
		redirect(base_url('DetailMaterial'),'refresh');

	}

	public function FormUpdate($id_detail_material)
	{

		$material = $this->M_material->Read();
		$supplier = $this->M_supplier->Read();
		$DetailMaterial = $this->M_detail_material->Detail($id_detail_material);	
			$data = array(
				'DetailMaterial' => $DetailMaterial,
				'supplier'       => $supplier,
				'material'       => $material,
				'isi'            => "DetailMaterial/detail-material-edit");

		$this->load->view('Layout/layout', $data, FALSE);
		
	}

	public function Update()
	{
		$id_detail_material = $this->input->post('id_detail_material');
		$id_material        = $this->input->post('id_material');
		$id_supplier        = $this->input->post('id_supplier');
		$harga_material     = $this->input->post('harga_material');

		$data = array(
			'id_detail_material' => $id_detail_material,
			'id_material'    => $id_material,
			'id_supplier'    => $id_supplier,
			'harga_material' => $harga_material);

		$this->M_detail_material->Update($data);
		$this->session->set_flashdata('sukses', ' Data Berhasil Di Edit');
		redirect(base_url('DetailMaterial'),'refresh');

	}

	public function Delete($id_detail_material)
	{
		$data = array('id_detail_material' => $id_detail_material);
		$this->M_detail_material->Delete($data);
		$this->session->flashdata('sukses', 'Data Telah Di Hapus !!!');

		redirect(base_url('DetailMaterial'),'refresh');
	}



}
