<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_login');
	}

	public function index()
	{
		$data = array('tittle' => 'Administrator | Log in');
		$this->load->view('login.php', $data, FALSE);

		$valid = $this->form_validation;

		$valid->set_rules('username','Username','required',
							array('required' => 'Username Harus Di Isi'));

		$valid->set_rules('password','Password','required',
							array('required' => 'Password Harap Di Isi'));


		if ($valid->run() === FALSE) {
			

		
		}else{

			$insert = $this->input;

			$username = $insert->post('username');
			$password = $insert->post('password');

			$check_login = $this->M_login->login($username,$password);

			if (count($check_login) == 1) {

				$this->session->set_userdata('username', $username);
				$this->session->set_userdata('id_login', $check_login->id_login);
				$this->session->set_userdata('nama_depan', $check_login->nama_depan);
				$this->session->set_userdata('nama_belakang', $check_login->nama_belakang);

				redirect(base_url('Dashboard'), 'refresh');

			}else{

				$this->session->set_flashdata('sukses', 'Username Atau Password Tidak Cocok');
				redirect(base_url('Login'),'refresh');

			}
		}
	}

	public function logout()
	{

		$this->session->unset_userdata('username');
		// $this->session->set_userdata('password', $password);
		$this->session->unset_userdata('nama_depan');
		$this->session->unset_userdata('nama_belakang');
		//$this->session->unset_userdata('jabatan');

		$this->session->set_flashdata('sukses', 'Anda Berhasil Logout');
		redirect(base_url('Login'), 'refresh');
		
	}

}
