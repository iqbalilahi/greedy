<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_supplier');
	}

	public function index()
	{

		$supplier = $this->M_supplier->Read();
		$data = array(
			'supplier' => $supplier,
			'isi'      => "Supplier/supplier");
		$this->load->view('Layout/layout', $data, FALSE);
		
	}

	public function FormCreate()
	{
		$data = array(
			'isi' => "Supplier/supplier-tambah");
		$this->load->view('Layout/layout', $data, FALSE);
	}

	public function Create()
	{
		$kode_supplier   = $this->input->post('kode_supplier');
		$nama_supplier   = $this->input->post('nama_supplier');
		$no_telp         = $this->input->post('no_telp');
		$alamat_supplier = $this->input->post('alamat_supplier');

		$data = array(
			'kode_supplier'   => $kode_supplier,
			'nama_supplier'   => $nama_supplier,
			'no_telp'         => $no_telp,
			'alamat_supplier' => $alamat_supplier);

		$this->M_supplier->Create($data);
		$this->session->set_flashdata('sukses', ' Data Berhasil Di Tambahkan');
		redirect(base_url('Supplier'),'refresh');
	}

	public function FormUpdate($id_supplier)
	{
		$supplier = $this->M_supplier->Detail($id_supplier);
		$data = array(
			'supplier'  => $supplier,
			'isi' 		=> "Supplier/supplier-edit");
		$this->load->view('Layout/layout', $data, FALSE);
	}

	public function Update()
	{
		$id_supplier     = $this->input->post('id_supplier');
		$kode_supplier   = $this->input->post('kode_supplier');
		$nama_supplier   = $this->input->post('nama_supplier');
		$no_telp         = $this->input->post('no_telp');
		$alamat_supplier = $this->input->post('alamat_supplier');

		$data = array(
			'id_supplier'	  => $id_supplier,
			'kode_supplier'   => $kode_supplier,
			'nama_supplier'   => $nama_supplier,
			'no_telp'         => $no_telp,
			'alamat_supplier' => $alamat_supplier);

		$this->M_supplier->Update($data);
		$this->session->set_flashdata('sukses', ' Data Edit Di Tambahkan');
		redirect(base_url('Supplier'),'refresh');
	}

	public function Delete($id_supplier)
	{
		$data = array('id_supplier' => $id_supplier);
		$this->M_supplier->Delete($data);
		$this->session->flashdata('sukses', 'Data Telah Di Hapus !!!');

		redirect(base_url('Supplier'),'refresh');
	}

}
