<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Greedy extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_greedy');
	}

	public function index()
	{
		
		$greedy = $this->m_greedy->Read();
		$data = array(
			'greedy' => $greedy,
			'isi' 	 => "Greedy/greedy");

		$this->load->view('Layout/layout', $data, FALSE);
	}

}
