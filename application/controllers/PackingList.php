<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PackingList extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_packing');
		$this->load->model('M_barang');
		$this->load->model('M_detail_material');
		$this->load->model('M_kendaraan');
		$this->load->model('M_subcount');
	}

	public function index()
	{
		$packing = $this->M_packing->Read();
		$data = array(
			'packing' => $packing,
			'isi'     => "PackingList/packing-list");

		$this->load->view('Layout/layout', $data, FALSE);
		
	}

	public function FormCreate()
	{
		$pn               = $this->M_packing->PN();
		$pk               = $this->M_packing->PK();
		$barang           = $this->M_barang->Read();
		$kendaraan        = $this->M_kendaraan->Read();
		$detail_material  = $this->M_detail_material->Read();
		$detail_material1 = $this->M_detail_material->Read();
		$subcount 		  = $this->M_subcount->Read();
		$data = array(
			'subcount'		   => $subcount,
			'kendaraan'        => $kendaraan,
			'detail_material'  => $detail_material,
			'detail_material1' => $detail_material1,
			'barang'           => $barang,
			'pk'               => $pk,
			'pn'               => $pn,
			'isi'              => "PackingList/packing-list-tambah");

		$this->load->view('Layout/layout', $data, FALSE);
		
	}

	public function Create()
	{
		$no_packing_list = $this->input->post('no_packing_list');
		$part_number     = $this->input->post('part_number');
		$id_barang       = $this->input->post('id_barang');
		$id_subcount     = $this->input->post('id_subcount');
		$id_kendaraan    = $this->input->post('id_kendaraan');
		$qty             = $this->input->post('qty');
		$unit            = $this->input->post('unit');
		$tanggal         = $this->input->post('tanggal');

		$data = array(
			'no_packing_list' => $no_packing_list,
			'part_number'     => $part_number,
			'id_barang'       => $id_barang,
			'id_subcount'     => $id_subcount,
			'id_kendaraan'    => $id_kendaraan,
			'qty'             => $qty,
			'unit'            => $unit,
			'tanggal'         => $tanggal);

		$this->M_packing->Create($data);
		$this->session->set_flashdata('sukses', ' Data Berhasil Di Tambahkan');
		redirect(base_url('PackingList'),'refresh');		
	}

	public function FormCreateMaterial($id_packing_list)
	{
		$detail_material  = $this->M_detail_material->Read();
		$detail_material1 = $this->M_detail_material->Read();
		$packing		  = $this->M_packing->Detail($id_packing_list);
		$data = array(
			'packing' 		   => $packing,
			'detail_material'  => $detail_material,
			'detail_material1' => $detail_material1,
			'isi'              => "DetailPacking/create");

		$this->load->view('Layout/layout', $data, FALSE);

	}

	public function CreateDetailPackingList()
	{
		
    	$id_detail_material    = $this->input->post('id_detail_material');
    	$id_packing_list       = $this->input->post('id_packing_list');

    	for ($i=0; $i < count($this->input->post('id_detail_material')); $i++) { 
      
      	$data1 = array(
        	'id_packing_list'    => $id_packing_list[$i],
        	'id_detail_material' => $id_detail_material[$i]); 

      	$this->db->insert('detail_packing_list', $data1);
		//redirect('RekamMedis');
	  	$this->session->set_flashdata('sukses', ' Data Material Berhasil Di Tambahkan');
		}

	}

	public function FormUpdate($id_packing_list)
	{
		$pn               = $this->M_packing->PN();
		$pk               = $this->M_packing->PK();
		$barang           = $this->M_barang->Read();
		$kendaraan        = $this->M_kendaraan->Read();
		$detail_material  = $this->M_detail_material->Read();
		$detail_material1 = $this->M_detail_material->Read();
		$subcount 		  = $this->M_subcount->Read();
		$packing           = $this->M_packing->Detail($id_packing_list);
		$data = array(
			'packing'		   => $packing,
			'subcount'		   => $subcount,
			'kendaraan'        => $kendaraan,
			'detail_material'  => $detail_material,
			'detail_material1' => $detail_material1,
			'barang'           => $barang,
			'pk'               => $pk,
			'pn'               => $pn,
			'isi'              => "PackingList/packing-list-edit");

		$this->load->view('Layout/layout', $data, FALSE);
		
	}

	public function Update()
	{
		$id_packing_list = $this->input->post('id_packing_list');
		$no_packing_list = $this->input->post('no_packing_list');
		$part_number     = $this->input->post('part_number');
		$id_barang       = $this->input->post('id_barang');
		$id_subcount     = $this->input->post('id_subcount');
		$id_kendaraan    = $this->input->post('id_kendaraan');
		$qty             = $this->input->post('qty');
		$unit            = $this->input->post('unit');
		$tanggal         = $this->input->post('tanggal');

		$data = array(
			'id_packing_list' => $id_packing_list,
			'no_packing_list' => $no_packing_list,
			'part_number'     => $part_number,
			'id_barang'       => $id_barang,
			'id_subcount'     => $id_subcount,
			'id_kendaraan'    => $id_kendaraan,
			'qty'             => $qty,
			'unit'            => $unit,
			'tanggal'         => $tanggal);

		$this->M_packing->Update($data);
		$this->session->set_flashdata('sukses', ' Data Berhasil Di Edit');
		redirect(base_url('PackingList'),'refresh');		
	}

	public function Delete($id_packing_list)
	{
		$data = array('id_packing_list' => $id_packing_list);
		$this->M_packing->Delete($data);
		$this->session->set_flashdata('sukses1', 'Data Telah Di Hapus !!!');

		redirect(base_url('PackingList'),'refresh');
	}

	public function Cetak($id_packing_list)
	{
		//$this->load->library('mpdf');
		$data['cetak'] = $this->M_packing->Cetak($id_packing_list);
		$mpdf = new \Mpdf\Mpdf();
		$html = $this->load->view('PackingList/packing-cetak',$data,TRUE);
		// $this->load->view('admin/cetak-pendaftaran', $cetak, FALSE);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}


	
}
