<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subcount extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_subcount');
	}

	public function index()
	{
		$subcount = $this->M_subcount->Read();
		$data = array(
			'subcount' => $subcount,
			'isi'      => "Subcount/subcount");

		$this->load->view('Layout/layout', $data, FALSE);
		
	}

	public function FormCreate()
	{

		$data = array(
			'isi'      => "Subcount/subcount-tambah");

		$this->load->view('Layout/layout', $data, FALSE);
		
	}

	public function Create()
	{
		$nama_subcount   = $this->input->post('nama_subcount');
		$no_telpone      = $this->input->post('no_telpone');
		$alamat_subcount = $this->input->post('alamat_subcount');

		$data = array(
			'nama_subcount' 	=> $nama_subcount,
			'no_telpone'    	=> $no_telpone,
			'alamat_subcount' 	=> $alamat_subcount);

		$this->M_subcount->Create($data);
		$this->session->set_flashdata('sukses', ' Data Berhasil Di Tambahkan');
		redirect(base_url('Subcount'),'refresh');
	}

	public function FormUpdate($id_subcount)
	{
		$subcount = $this->M_subcount->Detail($id_subcount);
		$data = array(
			'subcount' => $subcount,
			'isi'      => "Subcount/subcount-edit");

		$this->load->view('Layout/layout', $data, FALSE);
	}

	public function Update()
	{
		$id_subcount     = $this->input->post('id_subcount');
		$nama_subcount   = $this->input->post('nama_subcount');
		$no_telpone      = $this->input->post('no_telpone');
		$alamat_subcount = $this->input->post('alamat_subcount');

		$data = array(
			'id_subcount'		=> $id_subcount,
			'nama_subcount' 	=> $nama_subcount,
			'no_telpone'    	=> $no_telpone,
			'alamat_subcount' 	=> $alamat_subcount);

		$this->M_subcount->Update($data);
		$this->session->set_flashdata('sukses', ' Data Berhasil Di Tambahkan');
		redirect(base_url('Subcount'),'refresh');
	}

	public function Delete($id_subcount)
	{
		$data = array('id_subcount' => $id_subcount);
		$this->M_subcount->Delete($data);
		$this->session->flashdata('sukses', 'Data Telah Di Hapus !!!');

		redirect(base_url('Subcount'),'refresh');
	}

}
