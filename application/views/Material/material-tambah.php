<section class="content-header">
  <h1>
  
  <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active">Create</li>
  </ol>
</section>
<div class="col-md-12">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Form Tambah Data Metrial</h3>
    </div>
    <?php echo form_open(base_url('Material/Create')); ?>
    <div class="box-body">
      <div class="form-group">
        <label for="exampleInputEmail1">Kode Part</label>
        <input type="text" class="form-control"  placeholder="Kode Part" name="kode_part" required="" value="<?php echo $kode ?>" readonly>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Nama Metrial</label>
        <input type="text" class="form-control"  placeholder="Nama Metrial" name="nama_material" required="">
      </div>
       <div class="form-group">
        <label for="exampleInputEmail1">Panjang Metrial</label>
        <input type="text" class="form-control"  placeholder="Panjang Metrial" name="panjang_material" required="">
      </div>
    </div>
    <div class="box-footer">
      <button type="submit" class="btn btn-primary fa fa-save"> Simpan</button>
      <a href="<?php echo base_url('Material') ?>"><button type="button" class="btn btn-danger fa fa-close"> Kembali</button></a>
    </div>
    <?php form_close(); ?>
  </div>
</div>