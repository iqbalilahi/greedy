<section class="content-header">
  <h1>
  
  <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active">Update</li>
  </ol>
</section>
<div class="col-md-12">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Form Edit Data Metrial</h3>
    </div>
    <?php echo form_open(base_url('Material/Update/'.$material->id_material)); ?>
    <div class="box-body">
      <input type="hidden" class="form-control"  placeholder="Kode Part" name="id_material" required="" value="<?php echo $material->id_material ?>">
      <div class="form-group">
        <label for="exampleInputEmail1">Kode Part</label>
        <input type="text" class="form-control"  placeholder="Kode Part" name="kode_part" value="<?php echo $material->kode_part ?>">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Nama Metrial</label>
        <input type="text" class="form-control"  placeholder="Nama Metrial" name="nama_material" value="<?php echo $material->nama_material ?>">
      </div>
       <div class="form-group">
        <label for="exampleInputEmail1">Panjang Metrial</label>
        <input type="text" class="form-control"  placeholder="Panjang Metrial" name="panjang_material" value="<?php echo $material->panjang_material ?>">
      </div>
    </div>
    <div class="box-footer">
      <button type="submit" class="btn btn-primary fa fa-save"> Simpan</button>
      <a href="<?php echo base_url('Metrial') ?>"><button type="button" class="btn btn-danger fa fa-close"> Kembali</button></a>
    </div>
    <?php form_close(); ?>
  </div>
</div>