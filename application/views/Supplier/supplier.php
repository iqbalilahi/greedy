<section class="content-header">
  <?php 

  if ($this->session->flashdata('sukses')) {
    
    echo '<div class="alert alert-success"><i class="fa fa-check"> </i>';
    echo $this->session->flashdata('sukses');
    echo '</div>';
  }

  ?>
   <h1>
        
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Data Supplier</li>
      </ol>
</section>
<div class="col-md-12">
  <a href="<?php echo base_url('Supplier/FormCreate') ?>"><button class="btn btn-primary btn-sm fa fa-plus"> Tambah Data</button></a>
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Data Supplier</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead style="background: #823ea7">
          <tr>
            <th style="width: 10px;text-align: center;ba">No</th>
            <th style="text-align: center;">Kode Supplier</th>
            <th style="text-align: center;">Nama Supplier</th>
            <th style="text-align: center;">No Telepon</th>
            <th style="text-align: center;">Alamat Supplier</th>
           <!--  <th style="text-align: center;">Rem</th>
            <th style="text-align: center;">Suspensi</th> -->
            <th style="text-align: center;">Opsi</th>
          </tr>
        </thead>

        <?php $no=1; foreach ($supplier as $supplier) { ?>
        <tr>
          <td style="text-align: center;"><?php echo $no; ?></td>
          <td style="text-align: center;"><?php echo $supplier->kode_supplier ?></td>
          <td style="text-align: center;"><?php echo $supplier->nama_supplier ?></td>
          <td style="text-align: center;"><?php echo $supplier->no_telp ?></td>
          <td style="text-align: center;"><?php echo $supplier->alamat_supplier ?></td>
           <!-- <td style="text-align: center;"><?php echo $sepeda->sub_kriteria_rem ?></td>
          <td style="text-align: center;"><?php echo $sepeda->sub_kriteria_suspensi ?></td>   -->
          <td style="text-align: center;">
            <a href="<?php echo base_url('Supplier/FormUpdate/'.$supplier->id_supplier) ?>"><button class="btn btn-primary btn-sm fa fa-edit"></button></a>
            <?php include 'supplier-delete.php'; ?>
          </td>
        </tr>
        <?php $no++; } ?>
      </table>
    </div>
  </div>
</div>
