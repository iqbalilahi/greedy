<section class="content-header">
  <h1>
  
  <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active">Update</li>
  </ol>
</section>
<div class="col-md-12">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Form Edit Data Supplier</h3>
    </div>
    <?php echo form_open(base_url('Supplier/Update/'.$supplier->id_supplier)); ?>
    <div class="box-body">
       <input type="hidden" class="form-control"  placeholder="Kode Supplier" name="id_supplier" value="<?php echo $supplier->id_supplier ?>">
      <div class="form-group">
        <label for="exampleInputEmail1">Kode Supplier</label>
        <input type="text" class="form-control"  placeholder="Kode Supplier" name="kode_supplier" value="<?php echo $supplier->kode_supplier ?>">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Nama Supplier</label>
        <input type="text" class="form-control"  placeholder="Nama Supplier" name="nama_supplier" value="<?php echo $supplier->nama_supplier ?>">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Nomor Telepone</label>
        <input type="number" class="form-control"  placeholder="Nomor Telepone" name="no_telp" value="<?php echo $supplier->no_telp ?>">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Alamat Supplier</label>
        <textarea class="form-control" name="alamat_supplier" placeholder="Alamat Supplier"><?php echo $supplier->alamat_supplier ?></textarea>
      </div>
    </div>
    <div class="box-footer">
      <button type="submit" class="btn btn-primary fa fa-save"> Simpan</button>
      <a href="<?php echo base_url('Supplier') ?>"><button type="button" class="btn btn-danger fa fa-close"> Kembali</button></a>
    </div>
    <?php form_close(); ?>
  </div>
</div>