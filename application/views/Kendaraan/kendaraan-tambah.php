<section class="content-header">
  <h1>
  
  <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active">Create</li>
  </ol>
</section>
<div class="col-md-12">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Form Tambah Data Kendaraan</h3>
    </div>
    <?php echo form_open(base_url('Kendaraan/Create')); ?>
    <div class="box-body">
      <div class="form-group">
        <label for="exampleInputEmail1">Nama Kendaraan</label>
        <input type="text" class="form-control"  placeholder="Nama Kendaraan" name="nama_kendaraan" required="">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Panjang Kendaraan</label>
        <input type="text" class="form-control"  placeholder="Panjang Kendaraan" name="panjang_kendaraan" required="">
      </div>
       <div class="form-group">
        <label for="exampleInputEmail1">Lebar Kendaraan</label>
        <input type="text" class="form-control"  placeholder="Lebar Kendaraan" name="lebar_kendaraan" required="">
      </div>
       <div class="form-group">
        <label for="exampleInputEmail1">Tinggi Kendaraan</label>
        <input type="text" class="form-control"  placeholder="Tinggi Kendaraan" name="tinggi_kendaraan" required="">
      </div>
    </div>
    <div class="box-footer">
      <button type="submit" class="btn btn-primary fa fa-save"> Simpan</button>
      <a href="<?php echo base_url('Kendaraan') ?>"><button type="button" class="btn btn-danger fa fa-close"> Kembali</button></a>
    </div>
    <?php form_close(); ?>
  </div>
</div>