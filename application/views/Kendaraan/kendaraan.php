<section class="content-header">
  <?php 

  if ($this->session->flashdata('sukses')) {
    
    echo '<div class="alert alert-success"><i class="fa fa-check"> </i>';
    echo $this->session->flashdata('sukses');
    echo '</div>';
  }

  ?>
   <h1>
        
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Data Kendaraan</li>
      </ol>
</section>
<div class="col-md-12">
  <a href="<?php echo base_url('Kendaraan/FormCreate') ?>"><button class="btn btn-primary btn-sm fa fa-plus"> Tambah Data</button></a>
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Data Kendaraan</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead style="background: #823ea7">
          <tr>
            <th style="width: 10px;text-align: center;ba">No</th>
            <th style="text-align: center;">Nama Kendaraan</th>
            <th style="text-align: center;">Panjang Kendaraan</th>
            <th style="text-align: center;">Lebar Kendaraan</th>
            <th style="text-align: center;">Tinggi Kendaraan</th>
            <th style="text-align: center;">Luas Kendaraan</th>
            <th style="text-align: center;">Opsi</th>
          </tr>
        </thead>

        <?php $no=1; foreach ($kendaraan as $kendaraan) { ?>
        <tr>
          <td style="text-align: center;"><?php echo $no; ?></td>
          <td style="text-align: center;"><?php echo $kendaraan->nama_kendaraan ?></td>
          <td style="text-align: center;"><?php echo $kendaraan->panjang_kendaraan." M2" ?></td>
          <td style="text-align: center;"><?php echo $kendaraan->lebar_kendaraan." M2" ?></td>
          <td style="text-align: center;"><?php echo $kendaraan->tinggi_kendaraan." M2" ?></td>
          <td style="text-align: center;"><?php echo $kendaraan->panjang_kendaraan * $kendaraan->lebar_kendaraan * $kendaraan->tinggi_kendaraan." M3" ?></td>
          <td style="text-align: center;">
            <a href="<?php echo base_url('Kendaraan/FormUpdate/'.$kendaraan->id_kendaraan) ?>"><button class="btn btn-primary btn-sm fa fa-edit"></button></a>
            <?php include 'kendaraan-delete.php'; ?>
          </td>
        </tr>
        <?php $no++; } ?>
      </table>
    </div>
  </div>
</div>
