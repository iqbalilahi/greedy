<section class="content-header">
  <h1>
  
  <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active">Update</li>
  </ol>
</section>
<div class="col-md-12">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title"> &nbsp;&nbsp;&nbsp;Form Edit Data Packing List</h3>
    </div>
    <?php echo form_open(base_url('PackingList/Update/'.$packing->id_packing_list)); ?>
    <div class="box-body">
      <input type="hidden" class="form-control" name="id_packing_list" value="<?php echo $packing->id_packing_list ?>">
      <div class="col-md-6">
        <div class="form-group">
          <label for="exampleInputEmail1">Part Number</label>
          <input type="text" class="form-control"  placeholder="Nama Barang" name="part_number" value="<?php echo $pn; ?>" readonly>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Barang</label>
          <select name="id_barang" class="form-control">
            <option value="0"> -- Pilih Barang --</option>
            <?php foreach ($barang as $barang) { ?>
            <option value="<?php echo $barang->id_barang ?>" <?php if ($barang->id_barang == $packing->id_barang) {
              echo "selected";
            } ?>> <?php echo $barang->nama_barang ?></option>
            <?php } ?>
          </select>
        </div>
        
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="exampleInputEmail1">NO Packing list</label>
          <input type="text" class="form-control"  placeholder="Jenis Barang" name="no_packing_list" value="<?php echo $pk; ?>" readonly>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Need QTY</label>
          <input type="number" class="form-control"  placeholder="Need Quantity" name="qty" value="<?php echo $packing->qty ?>">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="exampleInputEmail1">Unit</label>
          <input type="text" class="form-control"  placeholder="Jenis Barang" name="unit" value="PCS" readonly>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <?php $date = date("Y-m-d"); ?>
          <label for="exampleInputEmail1">Tanggal </label>
          <input type="text" class="form-control"  placeholder="Jenis Barang" name="tanggal" value="<?php echo $date ?>" readonly>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label for="exampleInputEmail1">Kendaraan</label>
          <select name="id_kendaraan" class="form-control">
            <option value="0"> -- Pilih Kendaraan --</option>
            <?php foreach ($kendaraan as $kendaraan) { ?>
            <option value="<?php echo $kendaraan->id_kendaraan ?>" <?php if ($kendaraan->id_kendaraan == $packing->id_kendaraan) {
              echo "selected";
            } ?>> <?php echo $kendaraan->nama_kendaraan ?></option>
            <?php } ?>
          </select>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label for="exampleInputEmail1">Subcount</label>
          <select name="id_subcount" class="form-control">
            <option value="0"> -- Pilih Subcount --</option>
            <?php foreach ($subcount as $subcount) { ?>
            <option value="<?php echo $subcount->id_subcount ?>" <?php if ($subcount->id_subcount == $packing->id_subcount) {
              echo "selected";
            } ?>> <?php echo $subcount->nama_subcount ?></option>
            <?php } ?>
          </select>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <!-- <div class="box-footer"> -->
            <button type="submit" class="btn btn-primary fa fa-save"> Simpan</button>
            <a href="<?php echo base_url('PackingList') ?>"><button type="button" class="btn btn-danger fa fa-close"> Kembali</button></a>
          <!-- </div> -->
        </div>
      </div>
    </div>
    <?php form_close(); ?>
  </div>
</div>