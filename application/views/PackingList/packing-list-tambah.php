<link rel="stylesheet" href="<?php echo base_url() ?>assets/add_row/bootstrap.min.css" />
<script src="<?php echo base_url() ?>assets/add_row/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/add_row/jquery.min.js"></script>
<section class="content-header">
  <h1>
  
  <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active">Create</li>
  </ol>
</section>
<div class="col-md-12">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title"> &nbsp;&nbsp;&nbsp;Form Tambah Data Packing List</h3>
    </div>
    <?php echo form_open(base_url('PackingList/Create')); ?>
    <div class="box-body">
      <div class="col-md-6">
        <div class="form-group">
          <label for="exampleInputEmail1">Part Number</label>
          <input type="text" class="form-control"  placeholder="Nama Barang" name="part_number" value="<?php echo $pn; ?>" readonly>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Barang</label>
          <select name="id_barang" class="form-control">
            <option value="0"> -- Pilih Barang --</option>
            <?php foreach ($barang as $barang) { ?>
            <option value="<?php echo $barang->id_barang ?>"> <?php echo $barang->nama_barang ?></option>
            <?php } ?>
          </select>
        </div>
        
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="exampleInputEmail1">NO Packing list</label>
          <input type="text" class="form-control"  placeholder="Jenis Barang" name="no_packing_list" value="<?php echo $pk; ?>" readonly>
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Need QTY</label>
          <input type="number" class="form-control"  placeholder="Need Quantity" name="qty">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="exampleInputEmail1">Unit</label>
          <input type="text" class="form-control"  placeholder="Jenis Barang" name="unit" value="PCS" readonly>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <?php $date = date("Y-m-d"); ?>
          <label for="exampleInputEmail1">Tanggal </label>
          <input type="text" class="form-control"  placeholder="Jenis Barang" name="tanggal" value="<?php echo $date ?>" readonly>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label for="exampleInputEmail1">Kendaraan</label>
          <select name="id_kendaraan" class="form-control">
            <option value="0"> -- Pilih Kendaraan --</option>
            <?php foreach ($kendaraan as $kendaraan) { ?>
            <option value="<?php echo $kendaraan->id_kendaraan ?>"> <?php echo $kendaraan->nama_kendaraan ?></option>
            <?php } ?>
          </select>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label for="exampleInputEmail1">Subcount</label>
          <select name="id_subcount" class="form-control">
            <option value="0"> -- Pilih Subcount --</option>
            <?php foreach ($subcount as $subcount) { ?>
            <option value="<?php echo $subcount->id_subcount ?>"> <?php echo $subcount->nama_subcount ?></option>
            <?php } ?>
          </select>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <!-- <div class="box-footer"> -->
            <button type="submit" class="btn btn-primary fa fa-save"> Simpan</button>
            <a href="<?php echo base_url('PackingList') ?>"><button type="button" class="btn btn-danger fa fa-close"> Kembali</button></a>
          </div>
       <!--  </div> -->
      </div>
    </div>
    <?php form_close(); ?>
  </div>
</div>