<!DOCTYPE html>
<html>
	<head>
		<title>Cetak Packing List</title>
	</head>
	<style type="text/css">
		table.table-style-three {
		font-family: verdana, arial, sans-serif;
		font-size: 11px;
		color: #333333;
		border-width: 1px;
		border-color: #3A3A3A;
		border-collapse: collapse;
		}
		table.table-style-three th {
		border-width: 1px;
		padding: 10px;
		border-style: solid;
		border-color: #c0c0c0;
		background-color:;
		color: black;
		}
		table.table-style-three tr:hover td {
		cursor: pointer;
		}
		table.table-style-three tr:nth-child(even) td{
		background-color: ;
		}
		table.table-style-three td {
		border-width: 1px;
		padding: 8px;
		border-style: solid;
		border-color: #c0c0c0;
		background-color: #ffffff;
		}
	</style>
	<body>
		<table border="" align="center">
			<tr>
				<td style="text-align: center;"><h2 style="text-align: center;">PT ASAHI BEST BASE INDONESIA</h2>MM2100 Industrial Town Block C-2 Cikarang Barat Bekasi 17520, Indonesia<br>Tel : (021) 8980022 (Hunting) Fax : (021) 8980162</td>
			</tr>
		</table>
		<hr>
		<h2 style="text-align: center;"><u>PACKING LIST MATERIAL</u></h2>
		<table class="table-style-three" border="">
		<thead>
			<tr>
				<td style="">P/N :</td>
				<td><?php echo  $cetak->part_number; ?></td>
				<th style="text-align: center;width: 300"></th>
				<td>Date :</td>
				<td><?php echo $cetak->tanggal ?></td>
				
			</tr>
			<tr>
				<td style="text-align: center;">No Packing List :</td>
				<td><?php echo $cetak->no_packing_list ?></td>
				<th style="text-align: center;width: 300"></th>	
				<td style="">QTY PO :</td>
				<td><?php echo  $cetak->qty; ?></td>
				
			</tr>
		</thead>
		<tbody>
	</table>
	<br>
	<table class="table-style-three" border="1">
		<?php $no=1; ?>
		<tr>
			<th>Proses</th>
			<th>Material</th>
			<th>Supplier</th>
			<th>Weight</th>
			<th>Price</th>
			<th>Total Weight</th>
			<th>Total Price</th>
		</tr>
		<tr>
			<td><?php echo $cetak->nama_barang; ?></td>
			<td><?php $a = $this->M_packing->GetPackingList1();foreach ($a as $key){if($key->id_packing_list == $cetak->id_packing_list){
              	echo $key->nama_material."<br><br>";}}?></td>
            <td><?php $b = $this->M_packing->GetPackingList1();foreach ($b as $key1){if($key1->id_packing_list == $cetak->id_packing_list){
            	echo $key1->nama_supplier."<br><br>";}}?></td>
        	<td style="text-align: center;"><?php $c = $this->M_packing->GetPackingList1();foreach ($c as $key2){if($key2->id_packing_list == $cetak->id_packing_list){
        	echo $key2->panjang_material."<br><br>";}}?></td>
        	<td style="text-align: center;"><?php $d = $this->M_packing->GetPackingList1();foreach ($d as $key3){if($key3->id_packing_list == $cetak->id_packing_list){
        	echo "Rp. ", number_format($key3->harga_material)."<br><br>";}}?></td>
        	<td style="text-align: center;"><?php $e = $this->M_packing->GetPackingList1();foreach ($e as $key4){if($key4->id_packing_list == $cetak->id_packing_list){
        	echo $key4->panjang_material * $cetak->qty."<br><br>";}}?></td>
        	<td><?php $f = $this->M_packing->GetPackingList1();foreach ($f as $key5){if($key5->id_packing_list == $cetak->id_packing_list){
        	$jumlah = $key5->panjang_material * $cetak->qty;
        	echo "Rp. ", number_format($jumlah * $key5->harga_material )."<br><br>";}}?></td>
		</tr>
		<?php $no++; ?>
	</table>
	<br>
	<hr>
	<table class="table-style-three" style="margin-left: 63%;">
		<tr>
			<th>PREPARE</th>
			<th>APPROVED</th>
			<th>SUBCOUNT</th>
		</tr>
		<tr>
			<td height="100px"></td>
			<td height="100px"></td>
			<td height="100px"></td>
		</tr>
		<tr>
			<th></th>
			<th></th>
			<th></th>
		</tr>
	</table>
		
	</body>
</html>