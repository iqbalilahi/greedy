<a type="button" data-toggle="modal" data-target="#myModal1<?php echo $packing->id_packing_list ?>" class="btn btn-warning btn-sm fa fa-eye"> </a>
<div id="myModal1<?php echo $packing->id_packing_list ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
  <div role="document" class="modal-dialog" >
    <div class="modal-content" style="width: 1000px;margin-left: -30%;">
      <div class="modal-header">
        <h4 id="exampleModalLabel" class="modal-title">No Packing List : <?php echo $packing->no_packing_list ?></h4>
        <h4 id="exampleModalLabel" class="modal-title">Part Number: <?php echo $packing->part_number ?></h4>
        
      </div>
      <div class="modal-body" >
        <section class="invoice">
          <div class="row">
            <div class="col-xs-12">
              <h2 class="page-header">
              <i class="fa fa-globe"></i> <?php echo $packing->nama_subcount ?>
              <small class="pull-right">Date: <?php echo $packing->tanggal ?></small>
              
              </h2>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12 table-responsive">    
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Qty</th>
                    <th>Unit</th>
                    <th>Product</th>
                    <th>Material</th>
                    <th>Weight</th>
                    <th>Harga</th>
                    <th>Supllier</th>
                    <th>Subcount</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><?php echo $packing->qty ?></td>
                    <td><?php echo $packing->unit ?></td>
                    <td><?php echo $packing->nama_barang ?></td>
                    <td>
                      <?php
                      $a = $this->M_packing->GetPackingList1();
                      foreach ($a as $key2){
                      if($key2->id_packing_list == $packing->id_packing_list){
                      echo $key2->nama_material."<br>";}
                      }?>
                    </td>
                     <td>
                      <?php
                      $d = $this->M_packing->GetPackingList1();
                      foreach ($d as $key5){
                      if($key5->id_packing_list == $packing->id_packing_list){
                      echo $key5->panjang_material."<br>";}
                      }?>
                    </td>

                     <td>
                      <?php
                      $c = $this->M_packing->GetPackingList1();
                      foreach ($c as $key3){
                      if($key3->id_packing_list == $packing->id_packing_list){
                      echo $key3->harga_material."<br>";}
                      }?>
                    </td>
                    <td><?php $b = $this->M_packing->GetPackingList1();foreach ($b as $key1){if($key1->id_packing_list == $packing->id_packing_list){echo $key1->nama_supplier."<br>";} }?></td>
                    <td><?php echo $packing->nama_subcount ?></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="row no-print">
            <div class="col-xs-12">
              <a href="<?php echo base_url('PackingList/Cetak/'.$packing->id_packing_list) ?>" target="_blank" class="btn btn-success"><i class="fa fa-print"></i> Print</a>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>