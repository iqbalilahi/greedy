<section class="content-header">
  <?php
  if ($this->session->flashdata('sukses')) {
  
  echo '<div class="alert alert-success"><i class="fa fa-check"> </i>';
    echo $this->session->flashdata('sukses');
  echo '</div>';
  }elseif ($this->session->flashdata('sukses1')) {
    echo '<div class="alert alert-warning"><i class="fa fa-check"> </i>';
    echo $this->session->flashdata('sukses1');
  echo '</div>';
  }
  ?>
  <h1>
  
  <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active">Packing List Material</li>
  </ol>
</section>
<div class="col-md-12">
  <a href="<?php echo base_url('PackingList/FormCreate') ?>"><button class="btn btn-primary btn-sm fa fa-plus"> Tambah Data</button></a>
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Packing List Material</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead style="background: #823ea7">
          <tr>
            <th style="width: 10px;text-align: center;ba">No</th>
            <th style="text-align: center; width: 120px;">Part Number</th>
            <th style="text-align: center; width: 200px;">No Packing List</th>
            <th style="text-align: center; width: 130px;">Barang</th>
            <th style="text-align: center; width: 100px;">QTY</th>
            <th style="text-align: center; width: 100px;">Unit</th>
            <th style="text-align: center; width: 200px;">Tanggal</th>
            <th style="text-align: center;">Opsi</th>
          </tr>
        </thead>
        <?php $no=1; foreach ($packing as $packing) { ?>
        <tr>
          <td style="text-align: center;"><?php echo $no; ?></td>
          <td style="text-align: center;"><?php echo $packing->part_number ?></td>
          <td style="text-align: center;"><?php echo $packing->no_packing_list ?></td>
          <td style="text-align: center;"><?php echo $packing->nama_barang ?></td>
          <td style="text-align: center;"><?php echo $packing->qty ?></td>
          <td style="text-align: center;"><?php echo $packing->unit ?></td>
          <td style="text-align: center;"><?php echo $date = date('d/F/Y', strtotime($packing->tanggal));  ?></td>
          <td style="text-align: center;">
            <div class="btn-group">
                  <button type="button" class="btn btn-success btn-sm">Action</button>
                  <button type="button" class="btn btn-success btn-sm  dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="<?php echo base_url('PackingList/FormUpdate/'.$packing->id_packing_list) ?>" class="fa fa-edit"> Update </a></li>
                    <li><a href="<?php echo base_url('PackingList/FormCreateMaterial/'.$packing->id_packing_list) ?>" class="fa fa-plus"> Create Material</a></li>
                    <!-- <li><a href="#" class="fa fa-edit"> Update Material</a></li> -->
                  </ul>
                </div>
            <?php include 'detail_packing.php'; ?>
            <?php include 'packing-list-delete.php'; ?>
          </td>
        </tr>
        <?php $no++; } ?>
      </table>
    </div>
  </div>
</div>