<html>
  <head>
    <title></title>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/add_row/bootstrap.min.css" />
    <script src="<?php echo base_url() ?>assets/add_row/bootstrap.min.js"></script>
    <script src="<?php echo base_url() ?>assets/add_row/jquery.min.js"></script>
  </head>
  <body>
    
    <section class="content">
      <?php
      if ($this->session->flashdata('sukses')) {
      
      echo '<div class="alert alert-success"><i class="fa fa-check"> </i>';
        echo $this->session->flashdata('sukses');
      echo '</div>';
      }
      ?>
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"> &nbsp;&nbsp;&nbsp;Form Tambah Data Packing List</h3>
            </div>
            <div class="form-group">
              <form name="add_name" id="add_name">
                <div class="table-responsive">
                  <div class="box-body">
                    <table class="table table-bordered" id="dynamic_field">
                      <tr>
                        <input type="hidden" name="id_packing_list[]" class="form-control name_list" value="<?php echo $packing->id_packing_list ?>">
                        <td><select name="id_detail_material[]" class="form-control name_list" required="">
                          <option value="0">-- Pilih Material --</option>
                          <?php foreach ($detail_material as $detail_material) { ?>
                          <option value="<?php echo $detail_material->id_detail_material ?>"><?php echo $detail_material->nama_material ?></option>
                          <?php } ?>
                        </select></td>
                        <td style="text-align: center;width: 50px;"><button type="button" name="add" id="add" class="btn btn-success fa fa-plus"></button></td>
                      </tr>
                    </table>
                    <br>
                    <input type="submit" name="submit" id="submit" class="btn btn-primary" value="Submit" />
                    <a href="<?php echo base_url('PackingList') ?>"><button class="btn btn-danger" type="button"> Kembali</button></a>
                  </div>
                  
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  </body>
</html>
<script>
$(document).ready(function(){
var i=1;
$('#add').click(function(){
i++;
$('#dynamic_field').append('<tr id="row'+i+'"><input type="hidden" name="id_packing_list[]" class="form-control name_list" value="<?php echo $packing->id_packing_list?>"><td><select name="id_detail_material[]" class="form-control name_list" required=""><option value="0">-- Pilih Material --</option><?php foreach ($detail_material1 as $detail_material1) { ?><option value="<?php echo $detail_material1->id_detail_material ?>"><?php  echo $detail_material1->nama_material?></option><?php } ?></select></td><td style="text-align:center"><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove fa fa-trash-o"></button></td></tr>');
});
});
$(document).on('click', '.btn_remove', function(){
var button_id = $(this).attr("id");
$('#row'+button_id+'').remove();
});
$('#submit').click(function(){
$.ajax({
url:"<?php echo base_url('PackingList/CreateDetailPackingList') ?>",
method:"POST",
data:$('#add_name').serialize(),
success:function(data)
{
window.location.href = "<?php echo base_url('PackingList') ?>";
}
});
return false;
});
</script>