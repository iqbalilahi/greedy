<section class="content-header">
  <?php
  if ($this->session->flashdata('sukses')) {
  
  echo '<div class="alert alert-success"><i class="fa fa-check"> </i>';
    echo $this->session->flashdata('sukses');
  echo '</div>';
  }
  ?>
  <h1>
  
  <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active">Algoritma Greedy</li>
  </ol>
</section>
<div class="col-md-12">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Perhitungan Algoritma Greedy</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead style="background: #a14dbd;color: #fff;">
          <tr>
            <th style="width: 10px;text-align: center;">No</th>
            <th style="width: 250px;text-align: center;">Material</th>
            <th style="width: 100px;text-align: center;">Pi</th>
            <th style="width: 100px;text-align: center;">Wi</th>
            <th style="text-align: center;width: 50px;">Luas Kendaraan</th>
            <!-- <th style="text-align: center; width: 120px;">(X1, X2, X3)</th> -->
            <th style="text-align: center; width: 100px;">Total Quantity</th>
            <th style="text-align: center; width: 130px;">Total Weight</th>
            <th style="text-align: center;">Optimasi</th>
          </tr>
        </thead>
        <?php $no=1; foreach ($greedy as $greedy) { 

          $z = $greedy->panjang_kendaraan * $greedy->lebar_kendaraan * $greedy->tinggi_kendaraan;
          ?>
        <tr>
         <td style="text-align: center;"><?php echo $no; ?></td>
         <td style="text-align: ;">
          <?php
          $a = $this->m_greedy->Read1();
          foreach ($a as $key ) {
            if ($greedy->id_packing_list == $key->id_packing_list) {
               echo $key->nama_material."<br>";
            }
          }
          ?>
            
          </td>
          <td style="text-align: ;">
          <?php
          $b = $this->m_greedy->Read1();
          foreach ($b as $key1 ) {
            if ($greedy->id_packing_list == $key1->id_packing_list) {
               $total_p = $key1->harga_material*$greedy->qty;
               echo $total_p."<br>";
            }
          }
          ?>
          </td>
          <td style="text-align: center;">
          <?php
          $c = $this->m_greedy->Read1();
          foreach ($c as $key2 ) {
            if ($greedy->id_packing_list == $key2->id_packing_list) {
               $total_w = $key2->panjang_material*$greedy->qty;
               echo $total_w."<br>";
            }
          }
          ?>
          </td>
          <td style="text-align: center;"><?php echo $greedy->panjang_kendaraan * $greedy->lebar_kendaraan * $greedy->tinggi_kendaraan." M3" ?></td>
         <!--  <td style="text-align: center;">
          <?php
          $d = $this->m_greedy->Read1();
          foreach ($d as $key3 ) {
            if ($greedy->id_packing_list == $key3->id_packing_list) {
               $total_x = $key3->harga_material*$greedy->qty;
               $total_y = $key3->panjang_material*$greedy->qty;
               $total_z = $total_x / $total_y;
               echo round($total_z,"2")."<br>";
            }
          }
          ?>
          </td> -->
          <td style="text-align: center;"><?php echo $greedy->qty*4; ?></td>
           <td style="text-align: center;">
          <?php
          $c = $this->m_greedy->Read1();
          $e=0;
          foreach ($c as $key2 ) {
            if ($greedy->id_packing_list == $key2->id_packing_list) {
               $total_w = $key2->panjang_material*$greedy->qty;
              $e +=$total_w;  
            }
            
          }

          echo $e;
          ?>
          </td>
          <td style="text-align: center;"><?php if ($e >= $z) {
            echo "Tidak Ada Transportasi Yang Tersedia";
          }elseif ($e <= $z) {
            echo $greedy->nama_kendaraan;
          } ?></td>
        </tr>
        <?php $no++; } ?>
      </table>
    </div>
  </div>
</div>