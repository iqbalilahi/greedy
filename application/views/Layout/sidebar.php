<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo base_url() ?>assets/template/dist/img/avatar.png" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>Administrator</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
      </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      <li><a href="<?php echo base_url('Dashboard') ?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
        <li class="treeview">
        <a href="#">
          <i class="fa fa-circle-o"></i>
          <span>Data Master</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo base_url('Barang') ?>"><i class="fa fa-circle-o"></i> Barang</a></li>
          <li><a href="<?php echo base_url('Material') ?>"><i class="fa fa-circle-o"></i> Material</a></li>
          <li><a href="<?php echo base_url('Supplier') ?>"><i class="fa fa-circle-o"></i> Supplier</a></li>
          <li><a href="<?php echo base_url('Kendaraan') ?>"><i class="fa fa-circle-o"></i> Kendaraan</a></li>
          <li><a href="<?php echo base_url('Subcount') ?>"><i class="fa fa-circle-o"></i> Subcount</a></li>
        </ul>
      </li>
      <li><a href="<?php echo base_url('DetailMaterial') ?>"><i class="fa fa-circle-o"></i> <span>Detail Material</span></a></li>
      <li><a href="<?php echo base_url('PackingList') ?>"><i class="fa fa-circle-o"></i> <span>Packing List</span></a></li>
      <li><a href="<?php echo base_url('Greedy') ?>"><i class="fa fa-circle-o"></i> <span>Algoritma Greedy</span></a></li>
    
      
    </ul>
  </section>
</aside>