
      <header class="main-header">
        <!-- Logo -->
        <a href="" class="logo" style="background-color: #823ea7">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>A</b>BBI</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>PT ASAHI BEST BASE INDONESIA</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" style="background-color:#823ea7">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button" style="background-color: #823ea7">
            <span class="sr-only" style="background-color: #823ea7">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu" style="background-color: #823ea7">
            <ul class="nav navbar-nav" style="background-color: #823ea7">
              
              
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="<?php echo base_url('Login/logout') ?>">
                  <!-- <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image"> -->
                  <span class="hidden-xs fa fa-sign-out"> Logout</span>
                </a>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <!--  <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li> -->
            </ul>
          </div>
        </nav>
      </header>