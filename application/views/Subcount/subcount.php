<section class="content-header">
  <?php 

  if ($this->session->flashdata('sukses')) {
    
    echo '<div class="alert alert-success"><i class="fa fa-check"> </i>';
    echo $this->session->flashdata('sukses');
    echo '</div>';
  }

  ?>
   <h1>
        
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Data Subcount</li>
      </ol>
</section>
<div class="col-md-12">
  <a href="<?php echo base_url('Subcount/FormCreate') ?>"><button class="btn btn-primary btn-sm fa fa-plus"> Tambah Data</button></a>
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Data Subcount</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead style="background: #823ea7">
          <tr>
            <th style="width: 10px;text-align: center;ba">No</th>
            <th style="text-align: center;">Nama Subcount</th>
            <th style="text-align: center;">Nomor Telepone</th>
            <th style="text-align: center;">Alamat Subcount</th>
            <!--  <th style="text-align: center;">Roda</th>
            <th style="text-align: center;">Rem</th>
            <th style="text-align: center;">Suspensi</th> -->
            <th style="text-align: center;">Opsi</th>
          </tr>
        </thead>

        <?php $no=1; foreach ($subcount as $subcount) { ?>
        <tr>
          <td style="text-align: center;"><?php echo $no; ?></td>
          <td style="text-align: center;"><?php echo $subcount->nama_subcount ?></td>
          <td style="text-align: center;"><?php echo $subcount->no_telpone ?></td>
          <td style="text-align: center;"><?php echo $subcount->alamat_subcount ?></td>
          <td style="text-align: center;">
            <a href="<?php echo base_url('Subcount/FormUpdate/'.$subcount->id_subcount) ?>"><button class="btn btn-primary btn-sm fa fa-edit"></button></a>
            <?php include 'subcount-delete.php'; ?>
          </td>
        </tr>
        <?php $no++; } ?>
      </table>
    </div>
  </div>
</div>
