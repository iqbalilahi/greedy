<section class="content-header">
  <h1>
  
  <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active">Update</li>
  </ol>
</section>
<div class="col-md-12">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Form Edit Data Subcount</h3>
    </div>
    <?php echo form_open(base_url('Subcount/Update/'.$subcount->id_subcount)); ?>
    <div class="box-body">
      <div class="form-group">
         <input type="hidden" class="form-control"  placeholder="Nama Subcount" name="id_subcount" value="<?php echo $subcount->id_subcount ?>">
        <label for="exampleInputEmail1">Nama Subcount</label>
        <input type="text" class="form-control"  placeholder="Nama Subcount" name="nama_subcount" value="<?php echo $subcount->nama_subcount ?>">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Nomor Telepone</label>
        <input type="number" class="form-control"  placeholder="Nomor Telepone" name="no_telpone" value="<?php echo $subcount->no_telpone ?>">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Alamat Subcount</label>
        <textarea class="form-control" name="alamat_subcount" placeholder="Alamat Subcount"><?php echo $subcount->alamat_subcount ?></textarea>
      </div>
    </div>
    <div class="box-footer">
      <button type="submit" class="btn btn-primary fa fa-save"> Simpan</button>
      <a href="<?php echo base_url('Subcount') ?>"><button type="button" class="btn btn-danger fa fa-close"> Kembali</button></a>
    </div>
    <?php form_close(); ?>
  </div>
</div>