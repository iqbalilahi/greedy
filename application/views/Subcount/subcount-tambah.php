<section class="content-header">
  <h1>
  
  <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active">Create</li>
  </ol>
</section>
<div class="col-md-12">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Form Tambah Data Subcount</h3>
    </div>
    <?php echo form_open(base_url('Subcount/Create')); ?>
    <div class="box-body">
      <div class="form-group">
        <label for="exampleInputEmail1">Nama Subcount</label>
        <input type="text" class="form-control"  placeholder="Nama Subcount" name="nama_subcount" required="">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Nomor Telepone</label>
        <input type="number" class="form-control"  placeholder="Nomor Telepone" name="no_telpone" required="">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Alamat Subcount</label>
        <textarea class="form-control" name="alamat_subcount" placeholder="Alamat Subcount"></textarea>
      </div>
    </div>
    <div class="box-footer">
      <button type="submit" class="btn btn-primary fa fa-save"> Simpan</button>
      <a href="<?php echo base_url('Subcount') ?>"><button type="button" class="btn btn-danger fa fa-close"> Kembali</button></a>
    </div>
    <?php form_close(); ?>
  </div>
</div>