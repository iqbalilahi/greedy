<section class="content-header">
  <h1>
  
  <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active">Create</li>
  </ol>
</section>
<div class="col-md-12">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Form Tambah Data Detail Material</h3>
    </div>
    <?php echo form_open(base_url('DetailMaterial/Create')); ?>
    <div class="box-body">
      <div class="form-group">
        <label for="exampleInputEmail1">Material</label>
        <select name="id_material" class="form-control">
          <option value="0"> -- Pilih Material --</option>
          <?php foreach ($material as $material) { ?>
            <option value="<?php echo $material->id_material ?>"> <?php echo $material->nama_material ?></option>
          <?php } ?>
        </select>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Supplier</label>
        <select name="id_supplier" class="form-control">
          <option value="0"> -- Pilih Supplier --</option>
          <?php foreach ($supplier as $supplier) { ?>
            <option value="<?php echo $supplier->id_supplier ?>"> <?php echo $supplier->nama_supplier ?></option>
          <?php } ?>
        </select>
      </div>
      <div class="form-group">
        <label>Harga Material</label>
        <input type="text" name="harga_material" class="form-control" required="">
      </div>
    </div>
    <div class="box-footer">
      <button type="submit" class="btn btn-primary fa fa-save"> Simpan</button>
      <a href="<?php echo base_url('DetailMaterial') ?>"><button type="button" class="btn btn-danger fa fa-close"> Kembali</button></a>
    </div>
    <?php form_close(); ?>
  </div>
</div>