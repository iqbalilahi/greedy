<section class="content-header">
  <h1>
  
  <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active">Create</li>
  </ol>
</section>
<div class="col-md-12">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Form Tambah Data Barang</h3>
    </div>
    <?php echo form_open(base_url('Barang/Create')); ?>
    <div class="box-body">
      <div class="form-group">
        <label for="exampleInputEmail1">Nama Barang</label>
        <input type="text" class="form-control"  placeholder="Nama Barang" name="nama_barang" required="">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Jenis Barang</label>
        <input type="text" class="form-control"  placeholder="Jenis Barang" name="jenis_barang" required="">
      </div>
    </div>
    <div class="box-footer">
      <button type="submit" class="btn btn-primary fa fa-save"> Simpan</button>
      <a href="<?php echo base_url('Barang') ?>"><button type="button" class="btn btn-danger fa fa-close"> Kembali</button></a>
    </div>
    <?php form_close(); ?>
  </div>
</div>