<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_greedy extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function Read()
	{
		$this->db->select('*');
		$this->db->from('detail_packing_list');
		$this->db->join('packing_list', 'packing_list.id_packing_list = detail_packing_list.id_packing_list', 'left');
		$this->db->join('barang', 'barang.id_barang = packing_list.id_barang', 'left');
		$this->db->join('subcount', 'subcount.id_subcount = packing_list.id_subcount', 'left');
		$this->db->join('kendaraan', 'kendaraan.id_kendaraan = packing_list.id_kendaraan', 'left');
		$this->db->join('detail_material', 'detail_material.id_detail_material = detail_packing_list.id_detail_material', 'left');
		$this->db->join('material', 'material.id_material = detail_material.id_material', 'left');
		$this->db->group_by('packing_list.id_packing_list');
		$query = $this->db->get();
		return $query->result();
	}

	public function Read1()
	{
		$this->db->select('*');
		$this->db->from('detail_packing_list');
		$this->db->join('packing_list', 'packing_list.id_packing_list = detail_packing_list.id_packing_list', 'left');
		$this->db->join('barang', 'barang.id_barang = packing_list.id_barang', 'left');
		$this->db->join('subcount', 'subcount.id_subcount = packing_list.id_subcount', 'left');
		$this->db->join('kendaraan', 'kendaraan.id_kendaraan = packing_list.id_kendaraan', 'left');
		$this->db->join('detail_material', 'detail_material.id_detail_material = detail_packing_list.id_detail_material', 'left');
		$this->db->join('material', 'material.id_material = detail_material.id_material', 'left');
		$this->db->order_by('packing_list.id_packing_list');
		$query = $this->db->get();
		return $query->result();
	}

}
