<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_supplier extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function Create($data)
	{
		$this->db->insert('supplier', $data);
	}

	public function Read()
	{
		$this->db->select('*');
		$this->db->from('supplier');
		$this->db->order_by('id_supplier', 'ASC');

		$query = $this->db->get();
		return $query->result();
	}

	public function Detail($id_supplier)
	{
		$this->db->select('*');
		$this->db->from('supplier');
		$this->db->where('id_supplier', $id_supplier);
		$this->db->order_by('id_supplier', 'ASC');

		$query = $this->db->get();
		return $query->row();
	}

	public function Update($data)
	{
		$this->db->where('id_supplier', $data['id_supplier']);
		$this->db->update('supplier', $data);
	}

	public function Delete($data)
	{
		$this->db->where('id_supplier', $data['id_supplier']);
		$this->db->delete('supplier', $data);
	}

}
