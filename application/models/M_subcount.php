<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_subcount extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function Create($data)
	{
		$this->db->insert('subcount', $data);
	}

	public function Read()
	{
		$this->db->select('*');
		$this->db->from('subcount');
		$this->db->order_by('id_subcount', 'ASC');

		$query = $this->db->get();
		return $query->result();
	}

	public function Detail($id_subcount)
	{
		$this->db->select('*');
		$this->db->from('subcount');
		$this->db->where('id_subcount', $id_subcount);
		$this->db->order_by('id_subcount', 'ASC');

		$query = $this->db->get();
		return $query->row();
	}

	public function Update($data)
	{
		$this->db->where('id_subcount', $data['id_subcount']);
		$this->db->update('subcount', $data);
	}

	public function Delete($data)
	{
		$this->db->where('id_subcount', $data['id_subcount']);
		$this->db->delete('subcount', $data);
	}

	

}
