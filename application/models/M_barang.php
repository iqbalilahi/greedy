<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_barang extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function Create($data)
	{
		$this->db->insert('barang', $data);
	}

	public function Read()
	{
		$this->db->select('*');
		$this->db->from('barang');
		$this->db->order_by('id_barang', 'ASC');

		$query = $this->db->get();
		return $query->result();
	}

	public function Detail($id_barang)
	{
		$this->db->select('*');
		$this->db->from('barang');
		$this->db->where('id_barang', $id_barang);
		$this->db->order_by('id_barang', 'ASC');

		$query = $this->db->get();
		return $query->row();
	}

	public function Update($data)
	{
		$this->db->where('id_barang', $data['id_barang']);
		$this->db->update('barang', $data);
	}

	public function Delete($data)
	{
		$this->db->where('id_barang', $data['id_barang']);
		$this->db->delete('barang', $data);
	}

	

}
