<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_detail_material extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function Create($data)
	{
		$this->db->insert('detail_material', $data);
	}

	public function Read()
	{
		$this->db->select('*');
		$this->db->from('detail_material');
		$this->db->join('material', 'material.id_material = detail_material.id_material', 'left');
		$this->db->join('supplier', 'supplier.id_supplier = detail_material.id_supplier', 'left');
		$this->db->order_by('id_detail_material', 'ASC');
		$query = $this->db->get();
		return $query->result();
	}

	public function Detail($id_detail_material)
	{
		$this->db->select('*');
		$this->db->from('detail_material');
		$this->db->join('material', 'material.id_material = detail_material.id_material', 'left');
		$this->db->join('supplier', 'supplier.id_supplier = detail_material.id_supplier', 'left');
		$this->db->where('id_detail_material', $id_detail_material);
		$this->db->order_by('id_detail_material', 'ASC');
		$query = $this->db->get();
		return $query->row();
	}

	public function Update($data)
	{
		$this->db->where('id_detail_material', $data['id_detail_material']);
		$this->db->update('detail_material', $data);
	}

	public function Delete($data)
	{
		$this->db->where('id_detail_material', $data['id_detail_material']);
		$this->db->delete('detail_material', $data);
	}

}
