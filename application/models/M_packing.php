<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_packing extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function Create($data)
	{
		$this->db->insert('packing_list', $data);
	}

	public function Read()
	{
		$this->db->select('*');
		$this->db->from('packing_list');
		$this->db->join('barang', 'barang.id_barang = packing_list.id_barang', 'left');
		$this->db->join('subcount', 'subcount.id_subcount = packing_list.id_subcount', 'left');
		$this->db->join('kendaraan', 'kendaraan.id_kendaraan = packing_list.id_kendaraan', 'left');
		$this->db->group_by('id_packing_list', 'ASC');

		$query = $this->db->get();
		return $query->result();
	}


	public function GetPackingList1()
	{

		$this->db->select('*');
		$this->db->from('detail_packing_list');
		$this->db->join('packing_list', 'packing_list.id_packing_list = detail_packing_list.id_packing_list', 'left');
		$this->db->join('barang', 'barang.id_barang = packing_list.id_barang', 'left');
		$this->db->join('subcount', 'subcount.id_subcount = packing_list.id_subcount', 'left');
		$this->db->join('kendaraan', 'kendaraan.id_kendaraan = packing_list.id_kendaraan', 'left');

		$this->db->join('detail_material', 'detail_material.id_detail_material = detail_packing_list.id_detail_material', 'left');
		$this->db->join('material', 'material.id_material = detail_material.id_material', 'left');
		$this->db->join('supplier', 'supplier.id_supplier = detail_material.id_supplier', 'left');
		$this->db->order_by('packing_list.id_packing_list');

		$query = $this->db->get();
		return $query->result();
		
	}
	public function GetPackingList()
	{

		$this->db->select('*');
		$this->db->from('detail_packing_list');
		$this->db->join('packing_list', 'packing_list.id_packing_list = detail_packing_list.id_packing_list', 'left');
		$this->db->join('barang', 'barang.id_barang = packing_list.id_barang', 'left');
		$this->db->join('subcount', 'subcount.id_subcount = packing_list.id_subcount', 'left');
		$this->db->join('kendaraan', 'kendaraan.id_kendaraan = packing_list.id_kendaraan', 'left');

		$this->db->join('detail_material', 'detail_material.id_detail_material = detail_packing_list.id_detail_material', 'left');
		$this->db->join('material', 'material.id_material = detail_material.id_material', 'left');
		$this->db->join('supplier', 'supplier.id_supplier = detail_material.id_supplier', 'left');
		$this->db->group_by('packing_list.id_packing_list');


		$query = $this->db->get();
		return $query->result();
		
	}

	public function Detail($id_packing_list)
	{
		$this->db->select('*');
		$this->db->from('packing_list');
		$this->db->join('barang', 'barang.id_barang = packing_list.id_barang', 'left');
		$this->db->join('subcount', 'subcount.id_subcount = packing_list.id_subcount', 'left');
		$this->db->join('kendaraan', 'kendaraan.id_kendaraan = packing_list.id_kendaraan', 'left');
		$this->db->where('id_packing_list', $id_packing_list);
		$this->db->order_by('id_packing_list', 'ASC');

		$query = $this->db->get();
		return $query->row();
	}

	public function Cetak($id_packing_list)
	{

		$this->db->select('*');
		$this->db->from('detail_packing_list');
		$this->db->join('packing_list', 'packing_list.id_packing_list = detail_packing_list.id_packing_list', 'left');
		$this->db->join('barang', 'barang.id_barang = packing_list.id_barang', 'left');
		$this->db->join('subcount', 'subcount.id_subcount = packing_list.id_subcount', 'left');
		$this->db->join('kendaraan', 'kendaraan.id_kendaraan = packing_list.id_kendaraan', 'left');

		$this->db->join('detail_material', 'detail_material.id_detail_material = detail_packing_list.id_detail_material', 'left');
		$this->db->join('material', 'material.id_material = detail_material.id_material', 'left');
		$this->db->join('supplier', 'supplier.id_supplier = detail_material.id_supplier', 'left');
		$this->db->where('detail_packing_list.id_packing_list', $id_packing_list);


		$query = $this->db->get();
		return $query->row();
		
	}

	public function PN(){
	   $this->db->select('RIGHT(packing_list.part_number,2) as kode_part_number', FALSE);
	   $this->db->order_by('id_packing_list','DESC');    
	   $this->db->limit(1);    
	   $query = $this->db->get('packing_list');    
	   if($query->num_rows() <> 0){      
		       
		   $data = $query->row();      
		   $kode_part_number = intval($data->kode_part_number) + 1; 
	   }
	    else{      
		   $kode_part_number = 1; 
	   }
		  //$tgl=date('Y');
		  $kode_part_numbermax = str_pad($kode_part_number, 2, "0", STR_PAD_LEFT);    
		  $kode_part_numberjadi = "E3A"."-0115-".$kode_part_numbermax;  //format kode_part_number
		  return $kode_part_numberjadi;  
	  }

	  public function PK(){

	   $this->db->select('RIGHT(packing_list.no_packing_list,2) as kode_packing', FALSE);
	   $this->db->order_by('id_packing_list','DESC');    
	   $this->db->limit(1);    
	   $query1 = $this->db->get('packing_list');    
	   if($query1->num_rows() <> 0){      
		       
		   $data1 = $query1->row();      
		   $kode_packing = intval($data1->kode_packing) + 1; 
	   }
	    else{      
		   $kode_packing = 1; 
	   }
		  $thn=date('Y');
		  $bln=date('M');
		  $kode_packingmax = str_pad($kode_packing, 3, "0", STR_PAD_LEFT);    
		  $kode_packingjadi = $kode_packingmax."/PO/"."$bln"."/ABBI/"."$thn";  //format kode_packing
		  return $kode_packingjadi;  
	  }

	public function Delete($data)
	{
		$tables = array('packing_list', 'detail_packing_list');
		$this->db->where('id_packing_list', $data['id_packing_list']);
		$this->db->delete($tables);
	}

	public function Update($data)
	{
		$this->db->where('id_packing_list', $data['id_packing_list']);
		$this->db->update('packing_list', $data);
	}

	

}
