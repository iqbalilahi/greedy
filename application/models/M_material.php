<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_material extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function Create($data)
	{
		$this->db->insert('material', $data);
	}

	public function Read()
	{
		$this->db->select('*');
		$this->db->from('material');
		$this->db->order_by('id_material', 'ASC');

		$query = $this->db->get();
		return $query->result();
	}

	public function Detail($id_material)
	{
		$this->db->select('*');
		$this->db->from('material');
		$this->db->where('id_material', $id_material);
		$this->db->order_by('id_material', 'ASC');

		$query = $this->db->get();
		return $query->row();
	}

	public function Update($data)
	{
		$this->db->where('id_material', $data['id_material']);
		$this->db->update('material', $data);
	}

	public function Delete($data)
	{
		$this->db->where('id_material', $data['id_material']);
		$this->db->delete('material', $data);
	}

	public function Kode(){
	   $this->db->select('RIGHT(material.kode_part,2) as kode', FALSE);
	   $this->db->order_by('id_material','DESC');    
	   $this->db->limit(1);    
	   $query = $this->db->get('material');    
	   if($query->num_rows() <> 0){      
		       
		   $data = $query->row();      
		   $kode = intval($data->kode) + 1; 
	   }
	    else{      
		   $kode = 1; 
	   }
		  $tgl=date('Y');
		  $kodemax = str_pad($kode, 3, "0", STR_PAD_LEFT);    
		  $kodejadi = "100"."$tgl".$kodemax;  //format kode
		  return $kodejadi;  
	  }

	

}
