<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kendaraan extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function Create($data)
	{
		$this->db->insert('kendaraan', $data);
	}

	public function Read()
	{
		$this->db->select('*');
		$this->db->from('kendaraan');
		$this->db->order_by('id_kendaraan', 'ASC');

		$query = $this->db->get();
		return $query->result();
	}

	public function Detail($id_kendaraan)
	{
		$this->db->select('*');
		$this->db->from('kendaraan');
		$this->db->where('id_kendaraan', $id_kendaraan);
		$this->db->order_by('id_kendaraan', 'ASC');

		$query = $this->db->get();
		return $query->row();
	}

	public function Update($data)
	{
		$this->db->where('id_kendaraan', $data['id_kendaraan']);
		$this->db->update('kendaraan', $data);
	}

	public function Delete($data)
	{
		$this->db->where('id_kendaraan', $data['id_kendaraan']);
		$this->db->delete('kendaraan', $data);
	}

}
