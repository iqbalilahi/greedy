-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 01, 2019 at 03:39 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_greedy`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id_barang` int(11) NOT NULL,
  `nama_barang` varchar(25) NOT NULL,
  `jenis_barang` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_barang`, `nama_barang`, `jenis_barang`) VALUES
(1, 'WIRE', '-'),
(2, 'TUBE', '-');

-- --------------------------------------------------------

--
-- Table structure for table `detail_material`
--

CREATE TABLE `detail_material` (
  `id_detail_material` int(11) NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `id_material` int(11) NOT NULL,
  `harga_material` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_material`
--

INSERT INTO `detail_material` (`id_detail_material`, `id_supplier`, `id_material`, `harga_material`) VALUES
(1, 1, 6, '15000'),
(2, 2, 7, '20000'),
(3, 3, 8, '10000'),
(4, 1, 6, '25000');

-- --------------------------------------------------------

--
-- Table structure for table `detail_packing_list`
--

CREATE TABLE `detail_packing_list` (
  `id_detail_packing_list` int(11) NOT NULL,
  `id_packing_list` int(11) NOT NULL,
  `id_detail_material` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_packing_list`
--

INSERT INTO `detail_packing_list` (`id_detail_packing_list`, `id_packing_list`, `id_detail_material`) VALUES
(10, 9, 1),
(11, 9, 2),
(12, 9, 4),
(13, 9, 3),
(17, 13, 2),
(18, 13, 1),
(19, 13, 4);

-- --------------------------------------------------------

--
-- Table structure for table `kendaraan`
--

CREATE TABLE `kendaraan` (
  `id_kendaraan` int(11) NOT NULL,
  `nama_kendaraan` varchar(20) NOT NULL,
  `panjang_kendaraan` varchar(10) NOT NULL,
  `lebar_kendaraan` varchar(10) NOT NULL,
  `tinggi_kendaraan` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kendaraan`
--

INSERT INTO `kendaraan` (`id_kendaraan`, `nama_kendaraan`, `panjang_kendaraan`, `lebar_kendaraan`, `tinggi_kendaraan`) VALUES
(1, 'Hino 300 Series', '20', '10', '10'),
(2, 'Truck 480', '30', '20', '20');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id_login` int(11) NOT NULL,
  `nama_depan` varchar(20) NOT NULL,
  `nama_belakang` varchar(20) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id_login`, `nama_depan`, `nama_belakang`, `username`, `password`) VALUES
(1, 'Yulin', 'Indah', 'admin', 'admin'),
(2, 'Rian', 'adittia', 'rad', '12345'),
(3, 'Huday', 'arr', 'huday', '12345');

-- --------------------------------------------------------

--
-- Table structure for table `material`
--

CREATE TABLE `material` (
  `id_material` int(11) NOT NULL,
  `kode_part` varchar(10) NOT NULL,
  `nama_material` varchar(20) NOT NULL,
  `panjang_material` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `material`
--

INSERT INTO `material` (`id_material`, `kode_part`, `nama_material`, `panjang_material`) VALUES
(6, '1002019001', 'AVSS 0.5 OR/WH (N) L', '1.17'),
(7, '1002019002', 'AVSS 0.5 LGN (N) LF ', '1.22'),
(8, '1002019003', 'AVSS 0.5 GN/RD (N) L', '1.15');

-- --------------------------------------------------------

--
-- Table structure for table `packing_list`
--

CREATE TABLE `packing_list` (
  `id_packing_list` int(11) NOT NULL,
  `no_packing_list` varchar(25) NOT NULL,
  `part_number` varchar(15) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `id_subcount` int(11) NOT NULL,
  `id_kendaraan` int(11) NOT NULL,
  `qty` varchar(5) NOT NULL,
  `unit` varchar(15) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `packing_list`
--

INSERT INTO `packing_list` (`id_packing_list`, `no_packing_list`, `part_number`, `id_barang`, `id_subcount`, `id_kendaraan`, `qty`, `unit`, `tanggal`) VALUES
(9, '020/PO/Oct/ABBI/2019', 'E3A-0115-02', 2, 2, 1, '500', 'PCS', '2019-10-31'),
(13, '020/PO/Nov/ABBI/2019', 'E3A-0115-03', 1, 2, 2, '300', 'PCS', '2019-11-01');

-- --------------------------------------------------------

--
-- Table structure for table `subcount`
--

CREATE TABLE `subcount` (
  `id_subcount` int(11) NOT NULL,
  `nama_subcount` varchar(30) NOT NULL,
  `no_telpone` varchar(15) NOT NULL,
  `alamat_subcount` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subcount`
--

INSERT INTO `subcount` (`id_subcount`, `nama_subcount`, `no_telpone`, `alamat_subcount`) VALUES
(1, 'PT. Berkah Abadi', '021889921', 'Kawasan Pulo Gadung Jakarta Timur'),
(2, 'PT. Daito', '02189727781', 'Kawasan Jababeka, Cikarang Selatan');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id_supplier` int(11) NOT NULL,
  `kode_supplier` varchar(10) NOT NULL,
  `nama_supplier` varchar(30) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `alamat_supplier` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id_supplier`, `kode_supplier`, `nama_supplier`, `no_telp`, `alamat_supplier`) VALUES
(1, '3092', 'PT. Tekno Indonesia Sejahtera', '021890002', 'MM 2100, Jl Bali, Blok A'),
(2, '3091', 'PT. Rad International Teknolog', '0218899220', 'Jl. Jend Sudriman, Jakarta Selatan'),
(3, '3093', 'PT. PYG Corporation', '0219004737', 'Kawasan Jababeka Cikarang Barat');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `detail_material`
--
ALTER TABLE `detail_material`
  ADD PRIMARY KEY (`id_detail_material`),
  ADD KEY `id_supplier` (`id_supplier`),
  ADD KEY `id_material` (`id_material`);

--
-- Indexes for table `detail_packing_list`
--
ALTER TABLE `detail_packing_list`
  ADD PRIMARY KEY (`id_detail_packing_list`),
  ADD KEY `id_packing_list` (`id_packing_list`),
  ADD KEY `id_detail_material` (`id_detail_material`);

--
-- Indexes for table `kendaraan`
--
ALTER TABLE `kendaraan`
  ADD PRIMARY KEY (`id_kendaraan`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id_login`);

--
-- Indexes for table `material`
--
ALTER TABLE `material`
  ADD PRIMARY KEY (`id_material`);

--
-- Indexes for table `packing_list`
--
ALTER TABLE `packing_list`
  ADD PRIMARY KEY (`id_packing_list`),
  ADD KEY `id_barang` (`id_barang`),
  ADD KEY `id_subcount` (`id_subcount`),
  ADD KEY `id_kendaraan` (`id_kendaraan`);

--
-- Indexes for table `subcount`
--
ALTER TABLE `subcount`
  ADD PRIMARY KEY (`id_subcount`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id_supplier`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `detail_material`
--
ALTER TABLE `detail_material`
  MODIFY `id_detail_material` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `detail_packing_list`
--
ALTER TABLE `detail_packing_list`
  MODIFY `id_detail_packing_list` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `kendaraan`
--
ALTER TABLE `kendaraan`
  MODIFY `id_kendaraan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id_login` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `material`
--
ALTER TABLE `material`
  MODIFY `id_material` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `packing_list`
--
ALTER TABLE `packing_list`
  MODIFY `id_packing_list` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `subcount`
--
ALTER TABLE `subcount`
  MODIFY `id_subcount` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id_supplier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `detail_material`
--
ALTER TABLE `detail_material`
  ADD CONSTRAINT `detail_material_ibfk_1` FOREIGN KEY (`id_material`) REFERENCES `material` (`id_material`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `detail_material_ibfk_2` FOREIGN KEY (`id_supplier`) REFERENCES `supplier` (`id_supplier`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `detail_packing_list`
--
ALTER TABLE `detail_packing_list`
  ADD CONSTRAINT `detail_packing_list_ibfk_1` FOREIGN KEY (`id_packing_list`) REFERENCES `packing_list` (`id_packing_list`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `detail_packing_list_ibfk_2` FOREIGN KEY (`id_detail_material`) REFERENCES `detail_material` (`id_detail_material`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `packing_list`
--
ALTER TABLE `packing_list`
  ADD CONSTRAINT `packing_list_ibfk_1` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id_barang`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `packing_list_ibfk_3` FOREIGN KEY (`id_subcount`) REFERENCES `subcount` (`id_subcount`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
